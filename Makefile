
BUILD_TAG?=local
PUSH_TAG?=latest

build:
	docker build -t registry.gitlab.com/mydropwizard/lando-docker:$(BUILD_TAG) .

push: build
	docker tag registry.gitlab.com/mydropwizard/lando-docker:$(BUILD_TAG) registry.gitlab.com/mydropwizard/lando-docker:$(PUSH_TAG)
	docker push registry.gitlab.com/mydropwizard/lando-docker:$(PUSH_TAG)

.PHONY: build push

