FROM ubuntu:22.04

ENV DOCKER_VERSION 5:24.0.7-1~ubuntu.22.04~jammy
ENV LANDO_VERSION v3.20.6

RUN apt-get update && apt-get install -y \
    mysql-client \
	apt-transport-https \
    ca-certificates \
    curl \
    gnupg2 \
    unzip \
    sudo \
    software-properties-common \
 && rm -rf /var/lib/apt/lists/*

# Install Docker
RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - \
 && add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" \
 && apt-get update \
 # Set the docker group to a consistent value.
 && groupadd -g 5000 docker \
 && apt-get install -y docker-ce=$DOCKER_VERSION \
 && rm -rf /var/lib/apt/lists/* \
 && docker --version

# Install Lando
RUN curl -L -o /tmp/lando.deb https://github.com/lando/lando/releases/download/$LANDO_VERSION/lando-x64-$LANDO_VERSION.deb \
 && dpkg -i /tmp/lando.deb \
 && rm -f /tmp/lando.deb

# Copy script for making fake tty.
COPY faketty /usr/local/bin/

# Add a Lando user
RUN useradd -ms /bin/bash lando \
 && usermod -a -G docker lando \
 && echo 'lando ALL=(ALL) NOPASSWD:ALL' > /etc/sudoers.d/lando \
 && mkdir /home/lando/.lando \
 && mkdir /home/lando/.lando/plugins \
 && chown -R lando:lando /home/lando

# Copy our MTU plugin.
COPY mtu /home/lando/.lando/plugins/mtu

USER lando

# Attempt to force Lando to download docker-compose.
RUN lando version

WORKDIR /home/lando
